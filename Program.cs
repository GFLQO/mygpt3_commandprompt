﻿using System.Text;
using Newtonsoft.Json;

if (args.Length > 0)
{
    HttpClient client = new HttpClient();
    client.DefaultRequestHeaders.Add("authorization", "Bearer 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");
    var content = new StringContent("{\"model\": \"text-davinci-003\", \"prompt\": \"" + args[0] + "\",\"temperature\": 1,\"max_tokens\": 1000}", Encoding.UTF8, "application/json");
    HttpResponseMessage response = await client.PostAsync("https://api.openai.com/v1/completions", content);

    string responseString = await response.Content.ReadAsStringAsync();

    try
    {

        var resTraited = JsonConvert.DeserializeObject<dynamic>(responseString);

        string guess = GuessCommand(resTraited!.choices[0].text);
        Console.ForegroundColor = ConsoleColor.Green;
        Console.WriteLine($"---> My guess at the command prompt is: {guess}");
        Console.ResetColor();
    }
    catch (Exception e)
    {
        Console.WriteLine(e);
    }
}
else
{
    Console.WriteLine(" ---> you need to provide some input");
}

static string GuessCommand(string raw)
{
    Console.WriteLine("GTP 3 returned text : ");
    Console.ForegroundColor = ConsoleColor.Yellow;
    Console.WriteLine(raw);

    var lastIndex = raw.LastIndexOf('\n');
    string guess = raw.Substring(lastIndex + 1);

    Console.ResetColor();

    TextCopy.ClipboardService.SetText(guess);

    return guess;
}